<?php
	
require_once("../system/init.php");
$linenotify = new \pongsit\linenotify\linenotify();

parse_str($_SERVER['QUERY_STRING'], $queries);

if(empty($queries['code'])){
	$view = new \pongsit\view\view('message');
	$variables = array();
	$variables['message'] = 'เกิดข้อผิดพลาดกรุณาติดต่อผู้ให้บริการครับ';
	echo $view->create($variables);
	exit();
}

$fields = [
    'grant_type' => 'authorization_code',
    'code' => $queries['code'],
    'redirect_uri' => $confVariables['linenotify']['callback_uri'],
    'client_id' => $confVariables['linenotify']['client_id'],
    'client_secret' => $confVariables['linenotify']['client_secret']
];

try {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $confVariables['linenotify']['line_api_callback']);
    curl_setopt($ch, CURLOPT_POST, count($fields));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    $res = curl_exec($ch);

    curl_close($ch);

    if ($res == false)
        throw new Exception(curl_error($ch), curl_errno($ch));

    $jsons = json_decode($res,true);
    
    if(!empty($jsons['access_token'])){
    	$linenotify->insert_access_token($_SESSION['userId'],$jsons['access_token']);
    	$variables['message'] = '<img style="width:200px;" src="'.$path_to_core.'linenotify/img/bell-green.png"><br><span style="color:green;">เปิดระบบแจ้งเตือนเรียบร้อยครับ</span>';
    	$linenotify->push($jsons['access_token'],'เปิดระบบแจ้งเตือนเรียบร้อยครับ');
		header('Location: '.$path_to_core.'user/info.php?id='.$_SESSION['user']['id']);
		exit();
    }else{
        $view = new \pongsit\view\view('message');
        $variables = array();
        $variables['message'] = 'เกิดข้อผิดพลาดกรุณาติดต่อผู้ให้บริการครับ';
        echo $view->create($variables);
        exit(); 
    }
} catch(Exception $e) {
    $view = new \pongsit\view\view('message');
	$variables = array();
	$variables['message'] = 'เกิดข้อผิดพลาดกรุณาติดต่อผู้ให้บริการครับ';
	echo $view->create($variables);
	exit();
}