<?php

// CREATE TABLE `linenotify` (
//   `id` int(11) NOT NULL,
//   `userId` text NOT NULL,
//   `access_token` text NOT NULL,
//   `active` int(11) NOT NULL DEFAULT '1'
// ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

// --
// -- Indexes for dumped tables
// --

// --
// -- Indexes for table `linenotify`
// --
// ALTER TABLE `linenotify`
//   ADD PRIMARY KEY (`id`);

// --
// -- AUTO_INCREMENT for dumped tables
// --

// --
// -- AUTO_INCREMENT for table `linenotify`
// --
// ALTER TABLE `linenotify`
//   MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

namespace pongsit\linenotify;

class linenotify extends \pongsit\model\model{
	public function __construct(){
		parent::__construct();
	}
	
	function delete_access_token($userId){
		$results = $this->db->query_array0("select count(id) as count from ".$this->table." where userId='$userId';");
		if($results['count'] > 0){
			$this->db->delete($this->table,'userId="'.$userId.'"');
		}		
	}
	
	function insert_access_token($userId,$access_token){
		$this->delete_access_token($userId);	
		$this->db->insert($this->table,array('userId'=>$userId,'access_token'=>$access_token));
	}
	
	function get_access_token($userId){
		$query = "select access_token from ".$this->table." where userId='$userId' order by id desc limit 1;";
		$results = $this->db->query_array0($query);
		if(!empty($results['access_token'])){
			return $results['access_token'];
		}else{
			return false;
		}
	}
	
	function push_to_user($userId,$message){
		$access_token = $this->get_access_token($userId);
		if(!empty($access_token)){
			$this->push($access_token,$message);
		}
	}
	
	function push($access_token,$message){

		$headers = [
			'Authorization: Bearer ' . $access_token
		];

		$fields = [
			'message' => $message
		];

		try {
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $GLOBALS['confVariables']['linenotify']['line_api_push']);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_POST, count($fields));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			$res = curl_exec($ch);
			curl_close($ch);

			if ($res == false)
				throw new Exception(curl_error($ch), curl_errno($ch));

			$json = json_decode($res,true);
			return $json;
		} catch (Exception $e) {
			return $e;
		}
	}
}