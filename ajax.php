<?php
	
require_once("../system/init.php");
$linenotify = new \pongsit\linenotify\linenotify();

$results = array();

switch($_POST['type']){
	case 'off': 
		if(!empty($_SESSION['userId'])){
			$linenotify->delete_access_token($_SESSION['userId']);
			$results['done'] = 1;
		}
		break;
}

header('Content-Type: application/json');
echo json_encode($results);

