CREATE TABLE linenotify ( 
`id` INT AUTO_INCREMENT primary key NOT NULL,
`userId` TEXT NOT NULL, 
`access_token` TEXT NOT NULL,
`active` INT DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;