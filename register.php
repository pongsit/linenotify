<?php
	
require_once("../system/init.php");

if(empty($_SESSION['userId'])){
	header('Location: '.$path_to_core.'line/user-info.php');
	exit();
}

$queryStrings = [
    'response_type' => 'code',
    'client_id' => $confVariables['linenotify']['client_id'],
    'redirect_uri' => $confVariables['linenotify']['callback_uri'],
    'scope' => 'notify',
    'state' => 'abcdefg1234567'
];

$queryString = $confVariables['linenotify']['line_api_register'].'?'. http_build_query($queryStrings);

header('Location: '.$queryString);

